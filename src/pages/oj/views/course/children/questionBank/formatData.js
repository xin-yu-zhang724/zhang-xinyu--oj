//  paper 的标准模板数据
const singleData = {
  // 题目
  subject: '请填写单选题题目',
  // 单选选项
  options: [
    '选项A', '选项B', '选项C', '选项D'
  ],
  // 正确答案 学生考试生成的试卷答案为选项
  answer: '选项A',
  // 考生填写的答案
  studentAnswer: '',
  // 解析
  analysis: '',
  // 分值
  score: 5,
  selection_problem_type: 'single',
  // 是否正在编辑
  isEdit: false
}
const multipleData = {
  // 题目
  subject: '请填写多选题题目',
  // 多选选项
  options: [
    '选项A', '选项B', '选项C', '选项D'
  ],
  // 正确答案
  answer: ['选项A', '选项B'],
  // 考生的答案
  studentAnswer: '',
  // 解析
  analysis: '',
  // 分值
  score: 5,
  selection_problem_type: 'multiple',
  // 是否正在编辑
  isEdit: false
}
const judgeData = {
  // 题目
  subject: '请填写判断题题目',
  // 多选选项
  options: [
    '对',
    '错'
  ],
  // 答案
  answer: '对',
  // 考生的答案
  studentAnswer: '',
  // 解析
  analysis: '',
  // 分值
  score: 5,
  selection_problem_type: 'judge',
  // 是否正在编辑
  isEdit: false
}
const programmingData = {
  title: '',
  // 题目
  programmingQuestionId: -1,
  // 解析
  analysis: '',
  // 分值
  score: 5,
  // 是否正在编辑
  isEdit: false,
  programmingQuestionInfo: {}
}
// 整个试卷
const FormData = {
  title: '北软信息职业技术学院试卷模板',
  totalScore: 0,
  createPeople: '',
  singleData: [],
  multipleData: [],
  judgeData: [],
  programmingData: []
}
const preservation = {
  singleType: {
    subject: String,
    options: [
      {'A': 'a'}, {'B': 'b'}, {'C': 'c'}, {'D': 'd'}
    ],
    answer: Array,
    studentAnswer: Array,
    analysis: String,
    score: Number
  },
  multipleType: {
    // 题目
    subject: '',
    // 多选选项
    options: [
      'A', 'B', 'C', 'D', 'E'
    ],
    // 正确答案
    answer: Array,
    // 考生的答案
    studentAnswer: Array,
    // 解析
    analysis: String,
    // 分值
    score: Number
  },
  judgeType: {
    // 题目
    subject: String,
    // 答案 true / false
    answer: Boolean,
    // 考生的答案
    studentAnswer: Boolean,
    // 解析
    analysis: String,
    // 分值
    score: Number
  },
  subjectType: {
    // 题目
    subject: String,
    // 答案
    answer: String,
    // 考生的答案
    studentAnswer: String,
    // 解析
    analysis: String,
    // 分值
    score: Number
  }
}
export {
  singleData, multipleData, judgeData, programmingData, FormData, preservation
}
