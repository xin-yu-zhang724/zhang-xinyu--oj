const CourseList = () => import(/* webpackChunkName: "course" */ './CourseList.vue')
const CourseDetails = () => import(/* webpackChunkName: "course" */ './CourseDetails.vue')

export {CourseDetails, CourseList}
