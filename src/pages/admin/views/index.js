import Dashboard from './general/Dashboard.vue'
import Announcement from './general/Announcement.vue'
import User from './general/User.vue'
import Student from './general/Student.vue'
import Conf from './general/Conf.vue'
import JudgeServer from './general/JudgeServer.vue'
import PruneTestCase from './general/PruneTestCase.vue'
import Problem from './problem/Problem.vue'
import ProblemList from './problem/ProblemList.vue'
import ContestList from './contest/ContestList.vue'
import Contest from './contest/Contest.vue'
import Login from './general/Login.vue'
import Home from './Home.vue'
import ProblemImportOrExport from './problem/ImportAndExport.vue'
import Course from './course/Course.vue'
import CourseList from './course/CourseList.vue'
import CourseResourceList from './course/CourseResourceList'
import CourseResourceArrange from './course/CourseResourceArrange'
import Classroom from './course/Classroom'
import ClassroomList from './course/ClassroomList'
import ClassroomDataAnalysis from './course/ClassroomDataAnalysis'

export {
  Announcement, User, Conf, JudgeServer, Problem, ProblemList, Contest,
  ContestList, Login, Home, PruneTestCase, Dashboard, ProblemImportOrExport,
  Course, CourseList, CourseResourceArrange, CourseResourceList, Student,
  Classroom, ClassroomList, ClassroomDataAnalysis
}
