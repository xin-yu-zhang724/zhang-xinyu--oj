import Vue from 'vue'
import router from './router'
import axios from 'axios'
import utils from '@/utils/utils'

Vue.prototype.$http = axios
axios.defaults.baseURL = '/api'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'
axios.defaults.xsrfCookieName = 'csrftoken'

export default {
  // 登录
  login (username, password) {
    return ajax('login', 'post', {
      data: {
        username,
        password
      }
    })
  },
  logout () {
    return ajax('logout', 'get')
  },
  getProfile () {
    return ajax('profile', 'get')
  },
  // 获取公告列表
  getAnnouncementList (offset, limit) {
    return ajax('admin/announcement', 'get', {
      params: {
        paging: true,
        offset,
        limit
      }
    })
  },
  // 删除公告
  deleteAnnouncement (id) {
    return ajax('admin/announcement', 'delete', {
      params: {
        id
      }
    })
  },
  // 修改公告
  updateAnnouncement (data) {
    return ajax('admin/announcement', 'put', {
      data
    })
  },
  // 添加公告
  createAnnouncement (data) {
    return ajax('admin/announcement', 'post', {
      data
    })
  },
  // 获取用户列表
  getUserList (offset, limit, keyword) {
    let params = {paging: true, offset, limit}
    if (keyword) {
      params.keyword = keyword
    }
    return ajax('admin/user', 'get', {
      params: params
    })
  },
  // 获取单个用户信息
  getUser (id) {
    return ajax('admin/user', 'get', {
      params: {
        id
      }
    })
  },
  // 编辑用户
  editUser (data) {
    return ajax('admin/user', 'put', {
      data
    })
  },
  deleteUsers (id) {
    return ajax('admin/user', 'delete', {
      params: {
        id
      }
    })
  },
  importUsers (users) {
    return ajax('admin/user', 'post', {
      data: {
        users
      }
    })
  },
  generateUser (data) {
    return ajax('admin/generate_user', 'post', {
      data
    })
  },
  getLanguages () {
    return ajax('languages', 'get')
  },
  getSMTPConfig () {
    return ajax('admin/smtp', 'get')
  },
  createSMTPConfig (data) {
    return ajax('admin/smtp', 'post', {
      data
    })
  },
  editSMTPConfig (data) {
    return ajax('admin/smtp', 'put', {
      data
    })
  },
  testSMTPConfig (email) {
    return ajax('admin/smtp_test', 'post', {
      data: {
        email
      }
    })
  },
  getWebsiteConfig () {
    return ajax('admin/website', 'get')
  },
  editWebsiteConfig (data) {
    return ajax('admin/website', 'post', {
      data
    })
  },
  getJudgeServer () {
    return ajax('admin/judge_server', 'get')
  },
  deleteJudgeServer (hostname) {
    return ajax('admin/judge_server', 'delete', {
      params: {
        hostname: hostname
      }
    })
  },
  updateJudgeServer (data) {
    return ajax('admin/judge_server', 'put', {
      data
    })
  },
  getInvalidTestCaseList () {
    return ajax('admin/prune_test_case', 'get')
  },
  pruneTestCase (id) {
    return ajax('admin/prune_test_case', 'delete', {
      params: {
        id
      }
    })
  },
  createContest (data) {
    return ajax('admin/contest', 'post', {
      data
    })
  },
  getContest (id) {
    return ajax('admin/contest', 'get', {
      params: {
        id
      }
    })
  },
  editContest (data) {
    return ajax('admin/contest', 'put', {
      data
    })
  },
  getContestList (offset, limit, keyword) {
    let params = {paging: true, offset, limit}
    if (keyword) {
      params.keyword = keyword
    }
    return ajax('admin/contest', 'get', {
      params: params
    })
  },
  getContestAnnouncementList (contestID) {
    return ajax('admin/contest/announcement', 'get', {
      params: {
        contest_id: contestID
      }
    })
  },
  createContestAnnouncement (data) {
    return ajax('admin/contest/announcement', 'post', {
      data
    })
  },
  deleteContestAnnouncement (id) {
    return ajax('admin/contest/announcement', 'delete', {
      params: {
        id
      }
    })
  },
  updateContestAnnouncement (data) {
    return ajax('admin/contest/announcement', 'put', {
      data
    })
  },
  getProblemTagList () {
    return ajax('problem/tags', 'get')
  },
  compileSPJ (data) {
    return ajax('admin/compile_spj', 'post', {
      data
    })
  },
  createProblem (data) {
    return ajax('admin/problem', 'post', {
      data
    })
  },
  editProblem (data) {
    return ajax('admin/problem', 'put', {
      data
    })
  },
  deleteProblem (id) {
    return ajax('admin/problem', 'delete', {
      params: {
        id
      }
    })
  },
  getProblem (id) {
    return ajax('admin/problem', 'get', {
      params: {
        id
      }
    })
  },
  getProblemList (params) {
    params = utils.filterEmptyValue(params)
    return ajax('admin/problem', 'get', {
      params
    })
  },
  getContestProblemList (params) {
    params = utils.filterEmptyValue(params)
    return ajax('admin/contest/problem', 'get', {
      params
    })
  },
  getContestProblem (id) {
    return ajax('admin/contest/problem', 'get', {
      params: {
        id
      }
    })
  },
  createContestProblem (data) {
    return ajax('admin/contest/problem', 'post', {
      data
    })
  },
  editContestProblem (data) {
    return ajax('admin/contest/problem', 'put', {
      data
    })
  },
  deleteContestProblem (id) {
    return ajax('admin/contest/problem', 'delete', {
      params: {
        id
      }
    })
  },
  makeContestProblemPublic (data) {
    return ajax('admin/contest_problem/make_public', 'post', {
      data
    })
  },
  addProblemFromPublic (data) {
    return ajax('admin/contest/add_problem_from_public', 'post', {
      data
    })
  },
  getReleaseNotes () {
    return ajax('admin/versions', 'get')
  },
  getDashboardInfo () {
    return ajax('admin/dashboard_info', 'get')
  },
  getSessions () {
    return ajax('sessions', 'get')
  },
  exportProblems (data) {
    return ajax('export_problem', 'post', {
      data
    })
  },
  getClassroom (classroomId) {
    return ajax('admin/classroom', 'get', {
      params: {
        classroomId
      }
    })
  },
  createClassroom (data) {
    return ajax('admin/classroom', 'post', {
      data
    })
  },
  editClassroom (data) {
    return ajax('admin/classroom', 'put', {
      data
    })
  },
  getClassroomList (courseId, offset, limit, keyword) {
    let params = {paging: true, offset, limit, courseId}
    if (keyword) {
      params.keyword = keyword
    }
    return ajax('admin/classroom', 'get', {
      params: params
    })
  },
  deleteClassroom (classroomId) {
    return ajax('admin/classroom', 'delete', {
      params: {
        classroomId: classroomId
      }
    })
  },
  createCourse (data) {
    return ajax('admin/course', 'post', {
      data
    })
  },
  getCourseList (offset, limit, keyword) {
    let params = {paging: true, offset, limit}
    if (keyword) {
      params.keyword = keyword
    }
    return ajax('admin/course', 'get', {
      params: params
    })
  },
  getCourse (id) {
    return ajax('admin/course', 'get', {
      params: {
        id
      }
    })
  },
  editCourse (data) {
    return ajax('admin/course', 'put', {
      data
    })
  },
  createCourseResourceInfo (data) {
    return ajax('admin/course/resourceinfo', 'post', {
      data
    })
  },
  getResourceList (offset, limit, keyword, courseId) {
    let params = {paging: true, offset, limit, courseId}
    if (keyword) {
      params.keyword = keyword
    }
    return ajax('admin/course/resourceinfo', 'get', {
      params: params
    })
  },
  deleteCourseResource (id) {
    return ajax('admin/course/resourceinfo', 'delete', {
      params: {
        id
      }
    })
  },
  createCourseChapter (data) {
    return ajax('admin/course/chapter', 'post', {
      data
    })
  },
  getCourseChapter (id) {
    return ajax('admin/course/chapter', 'get', {
      params: {
        id
      }
    })
  },
  getCourseSection (course, chapter) {
    return ajax('admin/course/chapter/section', 'get', {
      params: {
        course,
        chapter
      }
    })
  },
  createCourseSection (data) {
    return ajax('admin/course/chapter/section', 'post', {
      data
    })
  },
  updateCourseChapter (data) {
    return ajax('admin/course/chapter', 'put', {
      data
    })
  },
  updateCourseSection (data) {
    return ajax('admin/course/chapter/section', 'put', {
      data
    })
  },
  deleteCourseChapter (id) {
    return ajax('admin/course/chapter', 'delete', {
      params: {
        id
      }
    })
  },
  deleteCourseSection (id) {
    return ajax('admin/course/chapter/section', 'delete', {
      params: {
        id
      }
    })
  },
  getCatalogResource (sectionId) {
    return ajax('admin/catalog/resource', 'get', {
      params: {
        sectionId
      }
    })
  },
  getCourseResourceInfo (courseId, keyword, genre) {
    let params = {courseId}
    if (keyword) {
      params.keyword = keyword
    }
    if (genre) {
      params.genre = genre
    }
    return ajax('admin/course/resourceinfo', 'get', {
      params: params
    })
  },
  matchSectionAndResource (data) {
    return ajax('admin/catalog/resource', 'post', {
      data
    })
  },
  delSectionResource (sectionId, resourceId) {
    return ajax('admin/catalog/resource', 'delete', {
      params: {
        sectionId,
        resourceId
      }
    })
  },
  importStudents (data) {
    return ajax('admin/user', 'post', {
      data
    })
  },
  getStudentList (offset, limit, keyword, adminType) {
    let params = {paging: true, offset, limit, adminType}
    if (keyword) {
      params.keyword = keyword
    }
    return ajax('admin/user', 'get', {
      params: params
    })
  },
  createStudent (data) {
    return ajax('admin/user', 'post', {
      data
    })
  },
  editStudent (data) {
    return ajax('admin/user', 'put', {
      data
    })
  },
  createUser (data) {
    return ajax('admin/user', 'post', {
      data
    })
  },
  // homework部分
  createHomework (data) {
    return ajax('admin/homework', 'post', {
      data
    })
  },
  updateHomework (data) {
    return ajax('admin/homework', 'put', {
      data
    })
  },
  deleteHomework (sectionId, homeworkId) {
    return ajax('admin/homework', 'delete', {
      params: {
        sectionId,
        homeworkId
      }
    })
  },
  getHomeworks (sectionId) {
    return ajax('admin/homework', 'get', {
      params: {
        sectionId
      }
    })
  },
  getHomeworkDetail (homeworkId) {
    return ajax('admin/homework', 'get', {
      params: {
        homeworkId
      }
    })
  },
  // selection problem
  createSelectionProblem (data) {
    return ajax('admin/selection_problem', 'post', {
      data
    })
  },
  updateSelectionProblem (data) {
    return ajax('admin/selection_problem', 'put', {
      data
    })
  },
  deleteSelectionProblem (homeworkId, selectionProblemId) {
    return ajax('admin/selection_problem', 'delete', {
      params: {
        homeworkId,
        selectionProblemId
      }
    })
  },
  getSelectionProblems (sectionId) {
    return ajax('admin/selection_problem', 'get', {
      params: {
        sectionId
      }
    })
  },
  // homework problem
  matchHomeworkAndProblem (data) {
    return ajax('admin/homework/problem', 'post', {
      data
    })
  },
  updateHomeworkAndProblem (data) {
    return ajax('admin/homework/problem', 'put', {
      data
    })
  },
  deleteProblemInHomework (homeworkId, problemId) {
    return ajax('admin/homework/problem', 'delete', {
      params: {
        homeworkId,
        problemId
      }
    })
  },
  getCourseHomeworks (courseId) {
    return ajax('admin/course/homeworks', 'get', {
      params: {
        courseId
      }
    })
  },
  getClassroomStudentsHomeworks (classroomId) {
    return ajax('admin/classroom/students/homeworks', 'get', {
      params: {
        classroomId
      }
    })
  }
}

/**
 * @param url
 * @param method get|post|put|delete...
 * @param params like queryString. if a url is index?a=1&b=2, params = {a: '1', b: '2'}
 * @param data post data, use for method put|post
 * @returns {Promise}
 */
function ajax (url, method, options) {
  if (options !== undefined) {
    var {params = {}, data = {}} = options
  } else {
    params = data = {}
  }
  return new Promise((resolve, reject) => {
    axios({
      url,
      method,
      params,
      data
    }).then(res => {
      // API正常返回(status=20x), 是否错误通过有无error判断
      if (res.data.error !== null) {
        Vue.prototype.$error(res.data.data)
        reject(res)
        // // 若后端返回为登录，则为session失效，应退出当前登录用户
        if (res.data.data.startsWith('Please login')) {
          router.push({name: 'login'})
        }
      } else {
        resolve(res)
        if (method !== 'get') {
          Vue.prototype.$success('Succeeded')
        }
      }
    }, res => {
      // API请求异常，一般为Server error 或 network error
      reject(res)
      Vue.prototype.$error(res.data.data)
    })
  })
}
