const merge = require('webpack-merge')
const devEnv = require('./dev.env')
process.argv[2] = '"http://10.70.25.36"'
let baseUrl = process.argv[2]

module.exports = merge(devEnv, {
  NODE_ENV: '"production"',
  baseUrl: baseUrl
})
