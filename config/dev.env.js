let date = require('moment')().format('YYYYMMDD')
let commit = require('child_process').execSync('git rev-parse HEAD').toString().slice(0, 5)
let version = `"${date}-${commit}"`

console.log(`current version is ${version}`)
process.argv[2] = '"http://10.70.25.36:8000"'
let baseUrl = process.argv[2]

module.exports = {
  NODE_ENV: '"development"',
  VERSION: version,
  USE_SENTRY: '0',
  baseUrl: baseUrl
}
