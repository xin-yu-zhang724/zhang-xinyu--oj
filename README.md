# NSIOJ Front End

[![vue](https://img.shields.io/badge/vue-2.5.13-blue.svg?style=flat-square)](https://github.com/vuejs/vue)
[![vuex](https://img.shields.io/badge/vuex-3.0.1-blue.svg?style=flat-square)](https://vuex.vuejs.org/)
[![echarts](https://img.shields.io/badge/echarts-3.8.3-blue.svg?style=flat-square)](https://github.com/ecomfe/echarts)
[![iview](https://img.shields.io/badge/iview-2.8.0-blue.svg?style=flat-square)](https://github.com/iview/iview)
[![element-ui](https://img.shields.io/badge/element-2.0.9-blue.svg?style=flat-square)](https://github.com/ElemeFE/element)
[![Build Status](https://travis-ci.org/QingdaoU/OnlineJudgeFE.svg?branch=master)](https://travis-ci.org/QingdaoU/OnlineJudgeFE)

>### A multiple pages app built for OnlineJudge. [Demo](https://10.70.5.169)

## 开始

首先安装 Node.js **v8.12.0** or **v14.15.1**。

### Linux

```bash
npm install
# we use webpack DllReference to decrease the build time,
# this command only needs execute once unless you upgrade the package in build/webpack.dll.conf.js
export NODE_ENV=development
npm run build:dll

# the dev-server will set proxy table to your backend
export TARGET=http://Your-backend

# serve with hot reload at localhost:8080
npm run dev
```
### Windows

```bash
npm install
# we use webpack DllReference to decrease the build time,
# this command only needs execute once unless you upgrade the package in build/webpack.dll.conf.js
set NODE_ENV=development
npm run build:dll

# the dev-server will set proxy table to your backend
set TARGET=http://Your-backend

# serve with hot reload at localhost:8080
npm run dev
```
## API

### User API - 用户管理相关API

```
1. name: importStudents // 导入学生信息
   method: post
   url: admin/user
   data: { users:[username, password, email, realname, grade, major, classes]} // 包含学生信息数组的数组

2. name: getStudentList // 获取全部学生信息
   method: get
   url: admin/user
   data: {offset, limit, keyword,"admin_type": "Regular User"} // 用户类型为普通用户
   response: [{id, username, real_name, grade, major, class_}, ……] // 包含全部学生对象的数组

3. name: createStudent // 创建一个学生
   method: post
   url: admin/user
   data: {  // 一个学生信息对象
       username, password, email, real_name, grade, major, class_
   }
   response: { // 成功提交的学生信息对象
       id, username, real_name, grade, major, class_
   }

4. name: getUser // 获取一个用户
   method: get
   url: admin/user
   data: {"id": 1} // 用户记录编号
   response: {...} // 一个学生信息对象或教师信息对象

5. name: editStudent // 编辑一个学生的信息
   method: put
   url: admin/user
   data: {
       "id": 1,
       "username": 11046,
       "password": user123, // 仅选中重置密码时传递
       "email": "11046@nsi.edu"
       "real_name": "xiadw"
       "grade": 2019,
       "major": "软件技术",
       "class_": 1,
       "problem_permission": None,
       "two_factor_auth": false,
       "open_api": false,
       "admin_type": "Regular User",
       "is_disabled": false
   }
   response: {...} // 一个学生对象

6. name: deleteUser // 删除一个用户的信息
   method: delete
   url: admin/user
   data: {"id": 1} // 用户信息记录编号

7. name: getUsers // 获取所有教师信息
   method: get
   url: admin/user
   data:  {offset, limit, keyword,"admin_type": "Admin"}
   response: [{id, username, email, real_name, create_time, last_login, admin_type}, ……] // 包含全部教师信息对象的数组

8. name: createUser // 创建一个教师
   method: post
   url: admin/user
   data: {username, password, email, real_name, "admin_type": "Admin"}
   response: {} // 教师信息对象

9. name: editUser // 编辑一个教师信息
    method: put
    url: admin/user
    data: {
       "id": 1,
       "username": 11046,
       "password": user123, // 仅选中重置密码时传递
       "email": "11046@nsi.edu"
       "real_name": "xiadw"
       "problem_permission": None,
       "tow_factor_auth": false,
       "open_api": false,
       "admin_type": "Admin",
       "is_disabled": false
    } // 教师信息对象
    response: {}  // 教师信息对象
```
## 打包发布方法

### Windows
```bash
# 设置环境
set NODE_DEV=production

# 打包
npm run build
```

将`dist`文件夹打包。在仓库创建发行版并将压缩包上传。

## Features

+ Webpack3 multiple pages with bundle size optimization
+ Easy use simditor & Nice codemirror editor
+ Amazing charting and visualization(echarts)
+ User-friendly operation
+ Quite beautiful：)

## Browser Support

Modern browsers and Internet Explorer 10+.

## LICENSE

[MIT](http://opensource.org/licenses/MIT)
