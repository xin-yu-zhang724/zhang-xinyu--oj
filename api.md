# API

## User API - 用户管理相关API

```
1. name: importStudents // 导入学生信息
   method: post
   url: admin/user
   data: { users:[username, password, email, realname, grade, major, classes]} // 包含学生信息数组的数组

2. name: getStudentList // 获取全部学生信息
   method: get
   url: admin/user
   data: {offset, limit, keyword,"admin_type": "Regular User"} // 用户类型为普通用户
   response: [{id, username, real_name, grade, major, class_}, ……] // 包含全部学生对象的数组

3. name: createStudent // 创建一个学生
   method: post
   url: admin/user
   data: {  // 一个学生信息对象
       username, password, email, real_name, grade, major, class_
   }
   response: { // 成功提交的学生信息对象
       id, username, real_name, grade, major, class_
   }

4. name: getUser // 获取一个用户
   method: get
   url: admin/user
   data: {"id": 1} // 用户记录编号
   response: {...} // 一个学生信息对象或教师信息对象

5. name: editStudent // 编辑一个学生的信息
   method: put
   url: admin/user
   data: {
       "id": 1,
       "username": 11046,
       "password": user123, // 仅选中重置密码时传递
       "email": "11046@nsi.edu"
       "real_name": "xiadw"
       "grade": 2019,
       "major": "软件技术",
       "class_": 1,
       "problem_permission": None,
       "two_factor_auth": false,
       "open_api": false,
       "admin_type": "Regular User",
       "is_disabled": false
   }
   response: {...} // 一个学生对象

6. name: deleteUser // 删除一个用户的信息
   method: delete
   url: admin/user
   data: {"id": 1} // 用户信息记录编号

7. name: getUsers // 获取所有教师信息
   method: get
   url: admin/user
   data:  {offset, limit, keyword,"admin_type": "Admin"}
   response: [{id, username, email, real_name, create_time, last_login, admin_type}, ……] // 包含全部教师信息对象的数组

8. name: createUser // 创建一个教师
   method: post
   url: admin/user
   data: {username, password, email, real_name, "admin_type": "Admin"}
   response: {} // 教师信息对象

9. name: editUser // 编辑一个教师信息
    method: put
    url: admin/user
    data: {
       "id": 1,
       "username": 11046,
       "password": user123, // 仅选中重置密码时传递
       "email": "11046@nsi.edu"
       "real_name": "xiadw"
       "problem_permission": None,
       "tow_factor_auth": false,
       "open_api": false,
       "admin_type": "Admin",
       "is_disabled": false
    } // 教师信息对象
    response: {}  // 教师信息对象
```

## Course API - 课程管理相关API

```
1. name: createCourseChapter // 创建课程目录章信息
   method: post
   url: admin/course/chapter
   data: {
       "course": 1,       // 课程编号ID
       "title": "第一章"   // 章标题
   }

2. name: createCourseSection // 创建课程目录节信息
   method: post
   url: admin/course/chapter/section
   data: {
       "course": 1,       // 课程编号ID
       "chapter": 1,      // 课程目录章编号ID
       "title": "第一节"  // 节标题
   }

3. name: getCourseChapter // 获取课程目录章信息
   method: get
   url: admin/course/chapter
   data: {
       "id": 1 // 课程编号ID
   }

4. name: getCourseSection // 获取课程目录节信息
   method: get
   url: admin/course/chapter/section
   data: {
       "course": 1  // 课程编号ID
       "chapter": 1 // 课程目录章编号
   }

5. name: deleteCourseChapter // 删除课程目录章信息
   method: delete
   url: admin/course/chapter
   data: {
       "id": 1 // 课程目录章编号ID
   }

6. name: deleteCourseSection  // 删除课程目录节信息
   method: delete
   url: admin/course/chapter/section
   data: {
       "id": 1 // 课程目录节编号ID
   }

7. name: updateCourseChapter // 更新课程目录章信息
   method: put
   url: admin/course/chapter
   data: {
       "id": 1, // 课程目录章编号ID
       "title": "new title" // 更新的课程目录章标题
   }

8. name: updateCourseSection // 更新课程目录节信息
   method: put
   url: admin/course/chapter/section
   data: {
       "id": 1,
       "title: "new title" // 更新的课程目录节标题
   }

9. name: getCourseResourceInfo // 获取课程资源信息
   method: get
   url: admin/course/resourceinfo
   data: {
       "courseId": 1 // 当前课程ID
   }

10. name: getCatalogResource // 获取课程章节资源信息
    method: get
    url: admin/catalog/resource
    data: {
        "sectionId": 1 // 当前课程节ID
    }

11. name: matchSectionAndResource // 匹配课程节与课程资源
    method: post
    url: admin/catalog/resource
    data: {
        "section": 1, // 当前节ID
        "recourse": 1, // 当前资源ID
    }

12. name: delSectionResource // 删除课程节的课程资源
    method: delete
    url: admin/catalog/resource
    data: {
        "sectionId": 1, // 当前课程节ID
        "resourceId": 1, // 当前课程节资源ID
    }

13. name: getVideoURL // 获取课程节对应视频资源的URL
    method: get
    url: api/course/resource
    data: {
        "resourceId": 1 // 当前课程节对应视频资源ID
    }

14. name: getSectionResources // 学生或教师获取课程指定节的全部资源信息
    methods: get
    url: api/course/resource
    data: {
        "sectionId": 1
    }

15. name: getHomework // 学生或教师获取指定作业中的全部题目
    method: get
    url: api/homework
    data: {
        "homeworkId": 1
    }

16. name: getSectionHomeworks // 学生或教师获取课程某节下的全部作业信息
    method: get
    url: api/homework
    data: {
        "sectionId": 1
    }
```

## Submission API - 题目提交相关API

### Public Program Problem Submission API - 公共程序设计题提交相关API

### Contest Program Problem Submission API - 竞赛程序设计题提交相关API

### Homework Program Problem Submission API - 作业程序设计题提交相关API

### Homework Selection Problem Submission API - 作业选择题提交相关API

### Homework Submissions API - 课程开课学生作业提交情况相关API

```
1. name: getHomeworkSubmissions // 学生获取当前作业中所有题目提交情况
   method: get
   url: api/homework_submissions
   data: {
       "homework_id": 1,
       "classroom_id": 1
   }
```

## Classroom API - 课堂相关API

### ClassroomStudentHomeworkStatus API - 学生在某次开课中某次作业的状态情况相关API

```
1. name: postClassroomStudentHomeworkStatus // 将当前学生在当前开课的当前作业的分数情况提交
   method: post
   url: api/classroom_student_homework
   data: {
       "classroomId": 1,
       "homeworkId": 1,
       "score": 85
   }
```

### ClassroomStudentVideoProgress API - 学生观看视频相关情况API

```
1. name: postClassroomStudentVideoProgress // 当学生点击播放时若无观看视频的数据记录则创建一条学生观看视频情况数据记录
   method: post
   url: api/classroom_student_video
   data: {
       "classroom_id": 1,
       "video_id": 1,
       "lastBrowseLocation": 30, // 秒数，整数
       "progress": 20 // 百分制整数
   }

2. name: putClassroomStudentVideoProgress // 当学生点击暂停视频或者离开视频观看界面时更新视频观看情况数据记录
   method: put
   url: api/classroom_studetn_video
   data: {
       "classroom_id": 1,
       "video_id": 1,
       "lastBrowseLocation": 30, // 秒数，整数
       "progress": 20 // 百分制整数
   }

3. name: getClassroomStudentVideoProgress // 当学生点击播放时尝试获取学生的观看记录
   method: get
   url: api/classroom_student_video
   data: {
       "classroom_id": 1,
       "video_id": 1
   }
```